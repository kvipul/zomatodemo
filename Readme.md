#Zomato Demo App
Development Time: 35Hrs (Not Continuously)

##Aim
Create an app that allows users to enter some text to search for restaurants
using [Zomato’s search API](https://developers.zomato.com/documentation#!/restaurant/search), and display the results in a scrollable list,
grouped by cuisine type. Each cuisine type should have a header labelling
the cuisine.

##Assumptions
Cuisines type and Restaurants have many to many relation, so if I group results by cuisine type there will be duplicate entries in many cuisines group.
Also some zomato api depend on user's location, so I'd to ask user for location first. Eventually I've asked for user's location first, then displayed category wise restaurants. User can search for chosen location and add some filters like required cuisine type, category type and sort option by price and rating.

##App Features
- One can search for restaurants with some useful filters like required cuisine type, category type and sort options
- Most of the restaurant detail will be visible on card itself, for more information about restaurant one can click the card
- More information about restaurant, restaurant menu and photo will be available via browser navigation
- Ripple effect is also provided for most of the events
- Views follow Material Design Guidelines
- GenericRequest is used to cache the api response for a few day, with this app will work in offline mode also (most of the time)
- Code has been written in separate modules to remove code duplication and maintain better code structure
- Material Vector Drawables are used for icons
- Proguard used for release build to make app size small and to make app more secure

##Third Party libraries and References
- Volley: For Network Call
- Picasso: To Load image in ImageView
- Android Support Library
- Zomato Developers Api: Data Source
- Some Files like GenericRequest.java, SessionManager.java, AppController.java are the files that I've been using for a very long time. Some section of these files may be from internet 