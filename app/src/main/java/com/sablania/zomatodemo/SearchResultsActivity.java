package com.sablania.zomatodemo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.sablania.zomatodemo.adapters.RestaurantsAdapter;
import com.sablania.zomatodemo.constants.Api;
import com.sablania.zomatodemo.constants.Constants;
import com.sablania.zomatodemo.models.FilterModel;
import com.sablania.zomatodemo.models.LocationModel;
import com.sablania.zomatodemo.models.RestaurantModel;
import com.sablania.zomatodemo.utils.AppController;
import com.sablania.zomatodemo.utils.GenericRequest;
import com.sablania.zomatodemo.utils.SessionManagerV2;

import java.util.ArrayList;

public class SearchResultsActivity extends AppCompatActivity {

    RestaurantsAdapter restaurantsAdapter;
    RecyclerView recyclerView;
    public static ArrayList<RestaurantModel.Restaurant> restaurantList;
    LocationModel location;
    String keyword;
    TextView noData;
    MenuItem menuItemFilter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        location = (LocationModel) getIntent().getSerializableExtra("location");
        keyword = getIntent().getStringExtra("search_keyword");
        Log.e("search_keyword", keyword);
        if (keyword.equalsIgnoreCase("bookmarks")) {
            getSupportActionBar().setTitle("Bookmarks");
        }else {
            getSupportActionBar().setTitle("\"" + keyword + "\" Results");
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        new SessionManagerV2(this).setFilterModel(new FilterModel());
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        noData = (TextView) findViewById(R.id.tv_no_data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }else if(id == R.id.action_add_filters){
            Intent intent = new Intent(SearchResultsActivity.this, FiltersActivity.class);
            intent.putExtra("location", location);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_filter_in_search, menu);
        menuItemFilter = menu.findItem(R.id.action_add_filters);
        if (keyword.equalsIgnoreCase("bookmarks")) {
            menuItemFilter.setVisible(false);
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (keyword.equalsIgnoreCase("bookmarks")) {
            restaurantList = new SessionManagerV2(this).getBookmarkedRestaurants();
            if (restaurantList.size() == 0) {
                noData.setVisibility(View.VISIBLE);
                return;
            }
            setRestaurantsAdapter();
        }else {
            searchForResult();
        }
    }

    private void searchForResult() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Results...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        String api = Api.SEARCH +"?";
        api = api + "entity_id=" + location.getEntityId()+"&entity_type=" +location.getEntityType();
        api = api + "&q=" + keyword +"&";
        FilterModel filterModel = new SessionManagerV2(this).getFilterModel();

        if (filterModel.getCuisines() != null && !filterModel.getCuisines().isEmpty()) {
            api = api + "cuisines=" + getStringIds(filterModel.getCuisines()) +"&";
        }

        if (filterModel.getCategories() != null && !filterModel.getCuisines().isEmpty()) {
            api = api + "categories=" + getStringIds(filterModel.getCategories()) +"&";
        }

        api = api + filterModel.getSort();

//        if(api.charAt(api.length()-1)=='&') {
//            api = api.substring(0, api.length() - 1);
//        }

        @SuppressWarnings("unchecked")
        GenericRequest genericRequest = new GenericRequest(Request.Method.GET,
                api, RestaurantModel.Restaurants.class, null, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                restaurantList = ((RestaurantModel.Restaurants) response).restaurantList;
                progressDialog.dismiss();

                if (restaurantList.size() == 0) {
                    noData.setVisibility(View.VISIBLE);
                    if(menuItemFilter!=null){
                        menuItemFilter.setVisible(false);
                    }
                    return;
                } else {
                    noData.setVisibility(View.GONE);
                    if(menuItemFilter!=null){
                        menuItemFilter.setVisible(true);
                    }
                }
                setRestaurantsAdapter();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.handleVolleyError(SearchResultsActivity.this, error, true);
                progressDialog.dismiss();
            }
        }, Constants.getHeader(), false);

        AppController.getInstance(this).addToRequestQueue(genericRequest, "restaurants");

    }

    private String getStringIds(ArrayList<Integer> arr) {
        String ids = "";
        for (int i=0; i<arr.size(); i++) {
            if (i == 0) {
                ids = arr.get(i) + "";
            } else {
                ids = ids+ "," + arr.get(i);
            }
        }
        return ids;
    }

    private void setRestaurantsAdapter() {
        restaurantsAdapter = new RestaurantsAdapter(SearchResultsActivity.this, restaurantList, true);
        recyclerView.setAdapter(restaurantsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

}
