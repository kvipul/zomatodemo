package com.sablania.zomatodemo.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sablania.zomatodemo.R;
import com.sablania.zomatodemo.SearchResultsActivity;
import com.sablania.zomatodemo.models.RestaurantModel;
import com.sablania.zomatodemo.utils.SessionManagerV2;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ViPul Sublaniya on 27-08-2017.
 */

public class RestaurantsAdapter extends RecyclerView.Adapter<RestaurantsAdapter.ViewHolder> {

    Context context;
    LayoutInflater inflater;
    ArrayList<RestaurantModel.Restaurant> list;
    SessionManagerV2 sessionManagerV2;
    boolean inSearchAcivity;
    public RestaurantsAdapter(Context context, ArrayList<RestaurantModel.Restaurant> list, boolean inSearchActivity) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sessionManagerV2 = new SessionManagerV2(context);
        this.inSearchAcivity = inSearchActivity;
    }

    @Override
    public RestaurantsAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View convertView = inflater.inflate(R.layout.cv_restaurant_detail, parent, false);
        RestaurantsAdapter.ViewHolder holder = new RestaurantsAdapter.ViewHolder(convertView);

        return holder;
    }

    @Override
    public void onBindViewHolder(RestaurantsAdapter.ViewHolder holder, final int position) {
        final RestaurantModel restaurantModel = list.get(position).getRestaurantModel();

        holder.tvRestaurantName.setText(restaurantModel.getName());
        holder.tvLocality.setText(restaurantModel.getLocation().getLocalityVerbose());
//        holder.tvBookmarks.setText(String.valueOf(restaurantModel.getBalance()));
        holder.tvRating.setText(String.valueOf(restaurantModel.getRating().getAggregateRating()));
        holder.tvRating.setBackgroundColor(Color.parseColor("#"+restaurantModel.getRating().getRatingColor()));
        holder.tvVotes.setText(String.valueOf(restaurantModel.getRating().getVotes())+" Votes");
        holder.tvCuisine.setText(restaurantModel.getCuisines());
        holder.tvAvgCost.setText(restaurantModel.getAvgCostForTwo()+restaurantModel.getCurrency());
        holder.tvSuggestionFromUs.setText(restaurantModel.getRating().getRatingText());
        if(restaurantModel.getFeaturedImage()!=null && !restaurantModel.getFeaturedImage().isEmpty()) {
            Picasso.with(context).load(restaurantModel.getFeaturedImage()).into(holder.image);
        }
        ArrayList<RestaurantModel.Restaurant> bookmarkedRestaurants = sessionManagerV2.getBookmarkedRestaurants();
        for (RestaurantModel.Restaurant restaurant : bookmarkedRestaurants) {
            if (restaurant.getRestaurantModel().getId() == restaurantModel.getId()) {
                holder.cbBookmark.setChecked(true);
                break;
            }
        }

        holder.cbBookmark.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sessionManagerV2.addBookmarkedRestaurant(list.get(position));
                } else {
                    sessionManagerV2.removeBookmarkedRestaurant(restaurantModel);
                    if(inSearchAcivity){
                        ((SearchResultsActivity)context).recreate();
                    }
                }
            }
        });
        holder.llSeeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(restaurantModel.getMenuUrl()));
                context.startActivity(browserIntent);
            }
        });
        holder.llSeePics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(restaurantModel.getPhotosUrl()));
                context.startActivity(browserIntent);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(restaurantModel.getUrl()));
                context.startActivity(browserIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvRestaurantName, tvLocality, tvBookmarks, tvRating, tvVotes, tvCuisine, tvAvgCost,
                tvSuggestionFromUs;
        ImageView image;
        LinearLayout llSeeMenu, llSeePics;
        AppCompatCheckBox cbBookmark;

        public ViewHolder(View itemView) {
            super(itemView);

            tvRestaurantName = (TextView) itemView.findViewById(R.id.tv_restaurant_name);
            tvLocality = (TextView) itemView.findViewById(R.id.tv_locality);
            tvBookmarks = (TextView) itemView.findViewById(R.id.tv_bookmarks);
            tvRating = (TextView) itemView.findViewById(R.id.tv_rating);
            tvVotes = (TextView) itemView.findViewById(R.id.tv_votes);
            tvCuisine = (TextView) itemView.findViewById(R.id.tv_cuisines);
            tvAvgCost = (TextView) itemView.findViewById(R.id.tv_avg_cost);
            tvSuggestionFromUs = (TextView) itemView.findViewById(R.id.tv_suggestion_from_us);
            image = (ImageView) itemView.findViewById(R.id.image);
            llSeeMenu = (LinearLayout) itemView.findViewById(R.id.ll_see_menu);
            llSeePics = (LinearLayout) itemView.findViewById(R.id.ll_see_pics);
            cbBookmark = (AppCompatCheckBox) itemView.findViewById(R.id.cb_bookmark);

        }
    }
}
