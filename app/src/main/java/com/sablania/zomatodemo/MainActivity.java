package com.sablania.zomatodemo;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.sablania.zomatodemo.adapters.RestaurantsAdapter;
import com.sablania.zomatodemo.constants.Api;
import com.sablania.zomatodemo.constants.Constants;
import com.sablania.zomatodemo.models.CategoryModel;
import com.sablania.zomatodemo.models.LocationModel;
import com.sablania.zomatodemo.models.RestaurantModel;
import com.sablania.zomatodemo.utils.AppController;
import com.sablania.zomatodemo.utils.GenericRequest;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private static ArrayList<CategoryModel.Categories> categoriesList;
    static LocationModel location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        location = (LocationModel) getIntent().getSerializableExtra("location");
        getSupportActionBar().setTitle(location.getTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fetchCuisineTypeAndSetAdapter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (categoriesList != null) {
            setupViewPagerAdapter();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
//        searchView.setBackground(getResources().getDrawable(R.color.md_white_1000));
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Intent intent = new Intent(MainActivity.this, SearchResultsActivity.class);
                intent.putExtra("search_keyword", query);
                intent.putExtra("location", location);
                startActivity(intent);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id==R.id.action_bookmarks) {
            Intent intent = new Intent(MainActivity.this, SearchResultsActivity.class);
            intent.putExtra("search_keyword", "bookmarks");
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void fetchCuisineTypeAndSetAdapter() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        @SuppressWarnings("unchecked")
        GenericRequest genericRequest = new GenericRequest(Request.Method.GET,
                Api.CATEGORIES, CategoryModel.CategoriesList.class, null, new Response.Listener() {
            @Override
            public void onResponse(Object response) {

                categoriesList = ((CategoryModel.CategoriesList) response).categoriesList;
                setupViewPagerAdapter();
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.handleVolleyError(MainActivity.this, error, true);
                progressDialog.dismiss();
            }
        }, Constants.getHeader(), false);

        AppController.getInstance(this).addToRequestQueue(genericRequest, "categories");
    }

    public void setupViewPagerAdapter() {
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        RecyclerView recyclerView;
        ProgressBar progressBar;
        TextView tvNoData;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
            progressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);
            tvNoData = (TextView) rootView.findViewById(R.id.tv_no_data);

            progressBar.setVisibility(View.VISIBLE);
            setCategoryResultsAdapter(categoriesList.get(getArguments().getInt(ARG_SECTION_NUMBER)).getCategoryModel().getId());
            return rootView;
        }

        private void setCategoryResultsAdapter(int categoryType) {
            String api = Api.SEARCH +"?";
            api = api + "entity_id=" + location.getEntityId()+"&entity_type=" +location.getEntityType()
                    +"&category=" + categoryType;

            @SuppressWarnings("unchecked")
            GenericRequest genericRequest = new GenericRequest(Request.Method.GET,
                    api, RestaurantModel.Restaurants.class, null, new Response.Listener() {
                @Override
                public void onResponse(Object response) {
                    ArrayList<RestaurantModel.Restaurant> restaurantList = ((RestaurantModel.Restaurants) response).restaurantList;
                    progressBar.setVisibility(View.GONE);

                    if (restaurantList.size() == 0) {
                        tvNoData.setVisibility(View.VISIBLE);
                        return;
                    }
                    RestaurantsAdapter restaurantsAdapter = new RestaurantsAdapter(getContext(), restaurantList, false);
                    recyclerView.setAdapter(restaurantsAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Constants.handleVolleyError(getContext(), error, true);
                }
            }, Constants.getHeader(), false);

            AppController.getInstance(getContext()).addToRequestQueue(genericRequest, "restaurants");

        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return categoriesList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return categoriesList.get(position).getCategoryModel().getName();
        }
    }
}
