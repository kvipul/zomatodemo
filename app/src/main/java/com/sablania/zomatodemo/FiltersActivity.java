package com.sablania.zomatodemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.sablania.zomatodemo.models.FilterModel;
import com.sablania.zomatodemo.models.LocationModel;
import com.sablania.zomatodemo.utils.SessionManagerV2;

import java.util.ArrayList;

public class FiltersActivity extends AppCompatActivity {
    CardView cvCuisinesType, cvCategoriesType;
    Spinner spnSortOptions;
    LocationModel location;
    Button btnApply;
    SessionManagerV2 sessionManagerV2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Choose Filters");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        location = (LocationModel) getIntent().getSerializableExtra("location");
        cvCuisinesType = (CardView) findViewById(R.id.cv_cuisines_type);
        cvCategoriesType = (CardView) findViewById(R.id.cv_categories_type);
        spnSortOptions = (Spinner) findViewById(R.id.spn_sort_option);
        btnApply = (Button) findViewById(R.id.btn_save);
        sessionManagerV2 = new SessionManagerV2(this);

        ArrayList<String> spnItem = new ArrayList<>();
        spnItem.add("Increasing in Cost");
        spnItem.add("Decreasing in Cost");
        spnItem.add("Increasing in Rating");
        spnItem.add("Decreasing in Rating");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spnItem);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spnSortOptions.setAdapter(dataAdapter);
        spnSortOptions.setSelection(0);

        cvCuisinesType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FiltersActivity.this, ChooseActivity.class);
                intent.putExtra("type", "Cuisines");
                intent.putExtra("location", location);
                startActivity(intent);
            }
        });

        cvCategoriesType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FiltersActivity.this, ChooseActivity.class);
                intent.putExtra("type", "Categories");
                intent.putExtra("location", location);
                startActivity(intent);
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sort = "";
                if (spnSortOptions.getSelectedItemPosition() == 0) {
                    sort = "sort=cost&order=asc";
                }else if (spnSortOptions.getSelectedItemPosition() == 1) {
                    sort = "sort=cost&order=desc";
                }else if (spnSortOptions.getSelectedItemPosition() == 2) {
                    sort = "sort=rating&order=asc";
                }else if (spnSortOptions.getSelectedItemPosition() == 3) {
                    sort = "sort=rating&order=desc";
                }

                if(!sort.isEmpty()) {
                    FilterModel filterModel = sessionManagerV2.getFilterModel();
                    filterModel.setSort(sort);
                    sessionManagerV2.setFilterModel(filterModel);
                }
                finish();
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.action_reset) {
            new SessionManagerV2(this).setFilterModel(new FilterModel());
            Toast.makeText(this, "Filter Reset!", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reset, menu);
        return true;
    }

}
