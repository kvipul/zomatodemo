package com.sablania.zomatodemo;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.sablania.zomatodemo.constants.Api;
import com.sablania.zomatodemo.constants.Constants;
import com.sablania.zomatodemo.models.CategoryModel;
import com.sablania.zomatodemo.models.CuisineModel;
import com.sablania.zomatodemo.models.FilterModel;
import com.sablania.zomatodemo.models.LocationModel;
import com.sablania.zomatodemo.utils.AppController;
import com.sablania.zomatodemo.utils.GenericRequest;
import com.sablania.zomatodemo.utils.SessionManagerV2;

import java.util.ArrayList;

public class ChooseActivity extends AppCompatActivity {

    ArrayList<DataItem> list;
    Button btnSave;
    LocationModel location;
    ListView listView;
    boolean cacheUsed = false;
    SessionManagerV2 sessionManagerV2;
    FilterModel filterModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);

        final String type = getIntent().getStringExtra("type");
        location = (LocationModel) getIntent().getSerializableExtra("location");

        getSupportActionBar().setTitle("Choose "+ type);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView) findViewById(R.id.recycler_view);
        btnSave = (Button) findViewById(R.id.btn_save);

        list = new ArrayList<>();
        sessionManagerV2 = new SessionManagerV2(this);
        filterModel = sessionManagerV2.getFilterModel();

        if(type.equalsIgnoreCase("Cuisines")){
            fetchCuisineTypeAndSetAdapter();
        }else{
            fetchCategoriesTypeAndSetAdapter();
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list == null || list.size()==0) {
                    return;
                }
                if (type.equalsIgnoreCase("Cuisines")) {
                    ArrayList<Integer> cuisines = new ArrayList<Integer>();
                    for (DataItem dataItem : list) {
                        if (dataItem.isCheckboxSelected()) {
                            cuisines.add(dataItem.getId());
                        }
                    }
                    filterModel.setCuisines(cuisines);
                } else {
                    ArrayList<Integer> categories = new ArrayList<Integer>();
                    for (DataItem dataItem : list) {
                        if (dataItem.isCheckboxSelected()) {
                            categories.add(dataItem.getId());
                        }
                    }
                    filterModel.setCategories(categories);
                }
                sessionManagerV2.setFilterModel(filterModel);
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void fetchCuisineTypeAndSetAdapter() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Categories...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        String api = Api.CUISINES + "?city_id=" + location.getCityId();

        @SuppressWarnings("unchecked")
        GenericRequest genericRequest = new GenericRequest(Request.Method.GET,
                api, CuisineModel.CuisinesList.class, null, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                if(cacheUsed) return;
                cacheUsed = true;

                ArrayList<CuisineModel.Cuisines> cuisinesList = ((CuisineModel.CuisinesList) response).cuisinesList;
                list.clear();
                for (CuisineModel.Cuisines cuisines : cuisinesList) {
                    DataItem dataItem = new DataItem();
                    dataItem.setItemName(cuisines.getCuisineModel().getName());
                    dataItem.setId(cuisines.getCuisineModel().getId());
                    dataItem.setCheckboxSelected(filterModel.getCuisines().contains(cuisines.getCuisineModel().getId()));
                    list.add(dataItem);
                }
                setAdapter();
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.handleVolleyError(ChooseActivity.this, error, true);
                progressDialog.dismiss();
            }
        }, Constants.getHeader(), false);

        AppController.getInstance(this).addToRequestQueue(genericRequest, "cuisines");
    }

    private void fetchCategoriesTypeAndSetAdapter() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Categories...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        @SuppressWarnings("unchecked")
        GenericRequest genericRequest = new GenericRequest(Request.Method.GET,
                Api.CATEGORIES, CategoryModel.CategoriesList.class, null, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                if(cacheUsed) return;
                cacheUsed = true;

                ArrayList<CategoryModel.Categories> categoriesList = ((CategoryModel.CategoriesList) response).categoriesList;
                list.clear();
                for (CategoryModel.Categories category : categoriesList) {
                    DataItem dataItem = new DataItem();
                    dataItem.setItemName(category.getCategoryModel().getName());
                    dataItem.setId(category.getCategoryModel().getId());
                    dataItem.setCheckboxSelected(filterModel.getCategories().contains(category.getCategoryModel().getId()));
                    list.add(dataItem);
                }
                setAdapter();
                progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.handleVolleyError(ChooseActivity.this, error, true);
                progressDialog.dismiss();
            }
        }, Constants.getHeader(), false);

        AppController.getInstance(this).addToRequestQueue(genericRequest, "categories");
    }

    private void setAdapter(){
        ArrayAdapter<DataItem> adapter = new ArrayAdapter<DataItem>(ChooseActivity.this, R.layout.choose_item, list){
            @NonNull
            @Override
            public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = getLayoutInflater().inflate(R.layout.choose_item, parent, false);
                TextView text1 = (TextView) view.findViewById(R.id.tv_item_text);
                AppCompatCheckBox checkBox = (AppCompatCheckBox) view.findViewById(R.id.checkbox);

//                text1.setText(list.get(position).getItemName()+ "  id=" +list.get(position).getId()
//                +" checked="+list.get(position).isCheckboxSelected());
                text1.setText(list.get(position).getItemName());
                checkBox.setChecked(list.get(position).isCheckboxSelected());

                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        list.get(position).setCheckboxSelected(isChecked);
                    }
                });
                return view;
            }

        };
        listView.setAdapter(adapter);
    }

    private class DataItem {
        int id;
        String itemName ;
        boolean checkboxSelected;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public boolean isCheckboxSelected() {
            return checkboxSelected;
        }

        public void setCheckboxSelected(boolean checkboxSelected) {
            this.checkboxSelected = checkboxSelected;
        }
    }

}
