package com.sablania.zomatodemo.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ViPul Sublaniya on 26-08-2017.
 */

public class LocationModel implements Serializable {

    @SerializedName("address")
    String address;

    @SerializedName("entity_id")
    String entityId;

    @SerializedName("entity_type")
    String entityType;

    @SerializedName("locality")
    String locality;

    @SerializedName("city")
    String city;

    @SerializedName("title")
    String title;

    @SerializedName("city_id")
    int cityId;

    @SerializedName("city_name")
    String cityName;

    @SerializedName("latitude")
    String latitude;

    @SerializedName("longitude")
    String longitude;

    @SerializedName("zipcode")
    String zipcode;

    @SerializedName("country_id")
    int countryId;

    @SerializedName("country_name")
    String countryName;

    @SerializedName("locality_verbose")
    String localityVerbose;

    public String getAddress() {
        return address;
    }

    public String getLocality() {
        return locality;
    }

    public String getCity() {
        return city;
    }

    public int getCityId() {
        return cityId;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getZipcode() {
        return zipcode;
    }

    public int getCountryId() {
        return countryId;
    }

    public String getLocalityVerbose() {
        return localityVerbose;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getCityName() {
        return cityName;
    }

    public String getTitle() {
        return title;
    }

    public String getEntityId() {
        return entityId;
    }

    public String getEntityType() {
        return entityType;
    }
}
