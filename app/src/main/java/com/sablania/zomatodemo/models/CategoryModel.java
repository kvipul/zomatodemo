package com.sablania.zomatodemo.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ViPul Sublaniya on 26-08-2017.
 */

public class CategoryModel implements Serializable {

    @SerializedName("id")
    int id;

    @SerializedName("name")
    String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public class Categories {
        @SerializedName("categories")
        CategoryModel categoryModel;

        public CategoryModel getCategoryModel() {
            return categoryModel;
        }
    }

    public class CategoriesList {
        @SerializedName("categories")
        public ArrayList<CategoryModel.Categories> categoriesList;
    }

}
