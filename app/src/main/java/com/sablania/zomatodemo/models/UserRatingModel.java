package com.sablania.zomatodemo.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ViPul Sublaniya on 27-08-2017.
 */

public class UserRatingModel implements Serializable {

    @SerializedName("aggregate_rating")
    String aggregateRating;

    @SerializedName("rating_text")
    String ratingText;

    @SerializedName("rating_color")
    String ratingColor;

    @SerializedName("votes")
    String votes;

    public String getAggregateRating() {
        return aggregateRating;
    }

    public String getRatingText() {
        return ratingText;
    }

    public String getRatingColor() {
        return ratingColor;
    }

    public String getVotes() {
        return votes;
    }
}
