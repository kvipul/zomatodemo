package com.sablania.zomatodemo.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ViPul Sublaniya on 26-08-2017.
 */

public class CuisineModel implements Serializable {

    @SerializedName("cuisine_id")
    int id;

    @SerializedName("cuisine_name")
    String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public class Cuisines {
        @SerializedName("cuisine")
        CuisineModel cuisineModel;

        public CuisineModel getCuisineModel() {
            return cuisineModel;
        }
    }

    public class CuisinesList {
        @SerializedName("cuisines")
        public ArrayList<CuisineModel.Cuisines> cuisinesList;
    }

}
