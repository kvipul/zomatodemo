package com.sablania.zomatodemo.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ViPul Sublaniya on 27-08-2017.
 */

public class FilterModel implements Serializable {

    ArrayList<Integer> cuisines = new ArrayList<>();
    ArrayList<Integer> categories = new ArrayList<>();
    String sort = "sort=cost&order=asc"; //default

    public ArrayList<Integer> getCuisines() {
        return cuisines;
    }

    public void setCuisines(ArrayList<Integer> cuisines) {
        this.cuisines = cuisines;
    }

    public ArrayList<Integer> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Integer> categories) {
        this.categories = categories;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
