package com.sablania.zomatodemo.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ViPul Sublaniya on 26-08-2017.
 */

public class RestaurantModel implements Serializable {

    @SerializedName("id")
    long id;

    @SerializedName("name")
    String name;

    @SerializedName("url")
    String url;

    @SerializedName("location")
    LocationModel location;

    @SerializedName("cuisines")
    String cuisines;

    @SerializedName("average_cost_for_two")
    int avgCostForTwo;

    @SerializedName("currency")
    String currency;

    @SerializedName("user_rating")
    UserRatingModel rating;

    @SerializedName("photos_url")
    String photosUrl;

    @SerializedName("menu_url")
    String menuUrl;

    @SerializedName("featured_image")
    String featuredImage;

    @SerializedName("deeplink")
    String deeplink;

    public class Restaurant {
        @SerializedName("restaurant")
        RestaurantModel restaurantModel;

        public RestaurantModel getRestaurantModel() {
            return restaurantModel;
        }
    }

    public class Restaurants {
        @SerializedName("restaurants")
        public ArrayList<RestaurantModel.Restaurant> restaurantList;
    }


    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public LocationModel getLocation() {
        return location;
    }

    public String getCuisines() {
        return cuisines;
    }

    public int getAvgCostForTwo() {
        return avgCostForTwo;
    }

    public String getCurrency() {
        return currency;
    }

    public UserRatingModel getRating() {
        return rating;
    }

    public String getPhotosUrl() {
        return photosUrl;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public String getFeaturedImage() {
        return featuredImage;
    }

    public String getDeeplink() {
        return deeplink;
    }
}
