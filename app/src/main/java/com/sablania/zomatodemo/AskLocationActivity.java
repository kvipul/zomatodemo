package com.sablania.zomatodemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.annotations.SerializedName;
import com.sablania.zomatodemo.utils.AppController;
import com.sablania.zomatodemo.utils.GenericRequest;
import com.sablania.zomatodemo.constants.Api;
import com.sablania.zomatodemo.constants.Constants;
import com.sablania.zomatodemo.models.LocationModel;

import java.util.ArrayList;

public class AskLocationActivity extends AppCompatActivity {
    ProgressBar progressBar;
    AutoCompleteTextView actvLocation;

    ArrayList<LocationModel> list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_location);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        actvLocation = (AutoCompleteTextView) findViewById(R.id.actv_location);

        actvLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                progressBar.setVisibility(View.VISIBLE);
                makeJsonRequestForFetchingLocationSuggestions(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        actvLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                actvLocation.setText(list.get(position).getCityName());
                progressBar.setVisibility(View.GONE);
                Constants.hideInputKeyboard(AskLocationActivity.this, actvLocation);
                Intent intent = new Intent(AskLocationActivity.this, MainActivity.class);
                intent.putExtra("location", list.get(position));
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        actvLocation.setText("");
        progressBar.setVisibility(View.GONE);

    }

    private void makeJsonRequestForFetchingLocationSuggestions(String s) {
        AppController.getInstance(this).cancelPendingRequests("locations");

        int noOfResult = 10;
        String api = Api.LOCATIONS + "?query=" + s + "&count=" + noOfResult;

        @SuppressWarnings("unchecked")
        GenericRequest genericRequest = new GenericRequest(Request.Method.GET,
                api, LocationSuggestionsList.class, null, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                list = ((LocationSuggestionsList)response).locationSuggestionsList;
                ArrayAdapter<LocationModel> adapter = new ArrayAdapter<LocationModel>(AskLocationActivity.this,
                        android.R.layout.simple_list_item_2, android.R.id.text1,list){
                    @NonNull
                    @Override
                    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                        View view = super.getView(position, convertView, parent);

                        TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                        TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                        text1.setText(list.get(position).getTitle());
                        text2.setText( list.get(position).getCountryName());

                        return view;
                    }
                };
                actvLocation.setAdapter(adapter);
                actvLocation.showDropDown();
                progressBar.setVisibility(View.GONE);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Constants.handleVolleyError(AskLocationActivity.this, error, true);
                progressBar.setVisibility(View.GONE);
            }
        }, Constants.getHeader(), false);

        AppController.getInstance(this).addToRequestQueue(genericRequest, "locations");
    }

    public class LocationSuggestionsList {
        @SerializedName("location_suggestions")
        ArrayList<LocationModel> locationSuggestionsList;
    }

}
