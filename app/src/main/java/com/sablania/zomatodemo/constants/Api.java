package com.sablania.zomatodemo.constants;


public class Api {

    public static String BASE_URL = "https://developers.zomato.com/api";

    public static String CATEGORIES = BASE_URL + "/v2.1/categories";
    public static String CUISINES = BASE_URL + "/v2.1/cuisines";
    public static String LOCATIONS = BASE_URL + "/v2.1/locations";
    public static String SEARCH = BASE_URL + "/v2.1/search";
}