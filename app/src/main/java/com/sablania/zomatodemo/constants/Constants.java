package com.sablania.zomatodemo.constants;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.util.HashMap;

public class Constants {

    public static HashMap<String, String> getHeader() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("user_key", "8da4940d941d3fc7a739503064f2a5bc"); //replace with your own key
        return headers;
    }

    public static void hideInputKeyboard(Activity activity, View view){
        //hide keyboard
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);

        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void handleVolleyError(Context context, VolleyError error, boolean showToast){
//        if(com.sablania.BuildConfig.DEBUG) {
//            if (error.networkResponse != null)
//                Log.e("Volley Error Code- ", error.networkResponse.statusCode + "");
//            Log.e("Volley Error Msg - ", error.toString() + " in " + context.getClass().getName());
//        }
        if (error instanceof NoConnectionError) {
            if(showToast){
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }else if(error instanceof TimeoutError){
            if(showToast){
                Toast.makeText(context, "Connection Timeout! Please try again.", Toast.LENGTH_SHORT).show();
            }
        }else{
            if(showToast){
                Toast.makeText(context, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static void disableAppBarDragCallback(AppBarLayout appBarLayout){
        // Disable "Drag" for AppBarLayout (i.e. User can't scroll appBarLayout by directly touching appBarLayout - User can only scroll appBarLayout by only using scrollContent)
        if (appBarLayout.getLayoutParams() != null) {
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
            AppBarLayout.Behavior appBarLayoutBehaviour = new AppBarLayout.Behavior();
            appBarLayoutBehaviour.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
                @Override
                public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                    return false;
                }
            });
            layoutParams.setBehavior(appBarLayoutBehaviour);
        }
    }

}

