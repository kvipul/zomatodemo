package com.sablania.zomatodemo.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sablania.zomatodemo.models.FilterModel;
import com.sablania.zomatodemo.models.RestaurantModel;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class SessionManagerV2 {

    // Sharedpref file name
    private static final String PREF_NAME = "SABLANIA_ZOMATO";
    private static final String KEY_FILTER = "FILTER";
//    private static final String KEY_FILTERED_CUISINES = "FILTERED_CUISINES";
//    private static final String KEY_FILTERED_CATEGORIES = "FILTERED_CATEGORIES";
    private static final String KEY_BOOKMARKED_RESTAURANT = "BOOKMARKED_RESTAURANT";
    private SharedPreferences pref;
    // Editor for Shared preferences
    private SharedPreferences.Editor editor;

    // Context
    private Context context;
    private int PRIVATE_MODE = 0;
    Gson gson;

    public SessionManagerV2(Context context) {
        this.context = context;
        pref = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        gson = new Gson();
    }

    public FilterModel getFilterModel() {
        String filterModelStr = pref.getString(KEY_FILTER, "");
        if(filterModelStr.isEmpty()){
            return new FilterModel();
        }
        FilterModel filterModel = gson.fromJson(filterModelStr, FilterModel.class);

        return filterModel;
    }

    public void setFilterModel(FilterModel filterModel) {
        String filterModelJson = gson.toJson(filterModel);

        editor.putString(KEY_FILTER, filterModelJson);
        editor.commit();
    }

    public ArrayList<RestaurantModel.Restaurant> getBookmarkedRestaurants() {
        String str = pref.getString(KEY_BOOKMARKED_RESTAURANT, "");
        if(str.isEmpty()){
            return new ArrayList<>();
        }
        Type listType = new TypeToken<ArrayList<RestaurantModel.Restaurant>>() {
        }.getType();
        return gson.fromJson(str, listType);
    }

    public void setBookmarkedRestaurants(ArrayList<RestaurantModel.Restaurant> bookmarkedRestaurants) {
        String string = gson.toJson(bookmarkedRestaurants);

        editor.putString(KEY_BOOKMARKED_RESTAURANT, string);
        editor.commit();
    }

    public void addBookmarkedRestaurant(RestaurantModel.Restaurant restaurant) {
        ArrayList<RestaurantModel.Restaurant> bookmarkedRestaurants = getBookmarkedRestaurants();
        bookmarkedRestaurants.add(restaurant);
        setBookmarkedRestaurants(bookmarkedRestaurants);
        Toast.makeText(context, "Added to Bookmarks", Toast.LENGTH_SHORT).show();
    }

    public void removeBookmarkedRestaurant(RestaurantModel restaurant) {
        ArrayList<RestaurantModel.Restaurant> bookmarkedRestaurants = getBookmarkedRestaurants();
        for(int i=0; i<bookmarkedRestaurants.size();i++){
            if(bookmarkedRestaurants.get(i).getRestaurantModel().getId()==restaurant.getId()){
                bookmarkedRestaurants.remove(i);
                break;
            }
        }
        setBookmarkedRestaurants(bookmarkedRestaurants);
        Toast.makeText(context, "Removed from Bookmarks", Toast.LENGTH_SHORT).show();

    }
}
